const form = document.createElement('form')
const container = document.getElementById('container')
const btn = document.createElement('button')
const table = document.createElement('table')
const tbody = document.createElement('tbody')
const filterWrapper = document.createElement('div')
const inpFilFio = document.createElement('input')
const inpFilStartLern = document.createElement('input')
const inpFilFaculty = document.createElement('input')
const inpFilEndLern = document.createElement('input')


filterWrapper.id = 'filterWrapper'
inpFilFio.placeholder = 'Поиск по ФИО'
inpFilFaculty.placeholder = 'Поиск по факультету'
inpFilStartLern.placeholder = 'Поиск по дате начала учебы'
inpFilEndLern.placeholder = 'Поиск по дате окончания учебы'
inpFilFio.classList.add('inp-search')
inpFilFaculty.classList.add('inp-search')
inpFilStartLern.classList.add('inp-search')
inpFilEndLern.classList.add('inp-search')
table.id = 'table'
table.setAttribute('border', '1')
table.setAttribute('cellspacing', '0')
table.setAttribute('cellpadding', '5')
form.id = 'form'
btn.id = 'btn'
btn.textContent = 'Добавить студента'
btn.type = 'button'
container.append(form)

let sortColumnFlag = ''

function createTable () {
  i = 1;
  const tr = document.createElement('tr')
  while (i <= 5) {
    const th = document.createElement('th')
    tr.append(th)
    th.id = 'heading' + i
      switch (i) {
        case (1):
          th.textContent = 'ФИО учащегося'
          break;
        case (2):
          th.textContent = 'Факультет'
          break;
        case (3):
          th.textContent = 'Дата рождения и возраст'
          break;
        case (4):
          th.textContent = 'Годы обучения и курс'
          break;
        case (5):
          th.textContent = 'Удалить студента'
          break;
      }
    i++
  }
 table.append(tr)
 container.append(table)
}
createTable();

function getYear(date) {
  const ageDifMs = Date.now() - date.getTime();
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function renderTime (date) {
  const endLern = date.getTime() + 126144000000;
  const resultTime = new Date(endLern)
  var dd = resultTime.getDate();
    if (dd < 10) dd = '0' + dd;

  var mm = resultTime.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

  var yy = resultTime.getFullYear();
    if (yy < 10) yy = '0' + yy;

  return yy + '-' + mm + '-' + dd;
}

function cours (date) {
  const res = new Date().getFullYear() - date.getFullYear()
    if (res > 4) {
      return 'Закончил'
    }
  return res
}

async function createTr (item) {
  const tr = document.createElement('tr')
  let i = 0;
  while (i <= 4) {
    const btnRemove = document.createElement('button')

    btnRemove.classList.add('btnRemove')
    btnRemove.id = item.id
    btnRemove.textContent = 'Удалить'

    btnRemove.addEventListener('click', async () => {
      await fetch(`http://localhost:3000/api/students/${btnRemove.id}`, {
          method: 'DELETE'
      })
      createStr ()
    })

    const td = document.createElement('td')
    td.classList.add('col')
    tr.append(td)
      switch (i) {
        case (0):
          td.textContent = item.FIO
          break;
        case (1):
          td.textContent = item.faculty
          break;
        case (2):
          td.textContent = item.birthday + ' Возраст: ' + getYear(new Date(item.birthday)) + ' лет'
          break;
        case (3):
          td.textContent = item.studyStart + ' - ' + item.endLern + ' Курс: ' + cours(new Date(item.studyStart))
        break;
        case (4):
          td.append(btnRemove)
        break;
      }
    i++
  }
  tbody.append(tr)
return tr
}

async function createStudent (name, lastname, surname, faculty, birthday, studyStart) {
      await fetch('http://localhost:3000/api/students', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify({
          name: name,
          lastname: lastname,
          surname: surname,
          faculty: faculty,
          birthday: birthday,
          studyStart: studyStart
      })
  });
}

async function createStr () {
  tbody.innerHTML = ''

  const response = await fetch('http://localhost:3000/api/students');
  let studentsList = await response.json();
  console.log(studentsList)

   if(sortColumnFlag == 'fio') {
      studentsList = studentsList.sort(( a, b ) => {
          if (a.FIO > b.FIO) {
              return 1
          }

          if (a.FIO < b.FIO) {
              return -1
          }

          return 0
      })
    }

    if(sortColumnFlag == 'faculty') {
      studentsList = studentsList.sort(( a, b ) => {
          if (a.faculty > b.faculty) {
              return 1
          }

          if (a.faculty < b.faculty) {
              return -1
          }

          return 0
      })
    }

    if(sortColumnFlag == 'birthday') {
      studentsList = studentsList.sort(( a, b ) => {
          if (a.birthday > b.birthday) {
              return 1
          }

          if (a.birthday < b.birthday) {
              return -1
          }

          return 0
      })
    }

    if(sortColumnFlag == 'studyStart') {
      studentsList = studentsList.sort(( a, b ) => {
          if (a.studyStart > b.studyStart) {
              return 1
          }

          if (a.studyStart < b.studyStart) {
              return -1
          }

          return 0
      })
    }
    sortColumnFlag = ''

  for (item of studentsList) {
    item.FIO = item.surname + ' ' + item.name + ' ' + item.lastname
    item.endLern = renderTime(new Date(item.studyStart));
  }

  if (inpFilFio !== '') {
    studentsList = studentsList.filter(function (item) {
    if (item.FIO.includes(inpFilFio.value)) return true
    })
  }

  if (inpFilFaculty !== '') {
    studentsList = studentsList.filter(function (item) {
    if (item.faculty.includes(inpFilFaculty.value)) return true
    })
  }

  if (inpFilStartLern !== '') {
    studentsList = studentsList.filter(function (item) {
    if (item.studyStart.includes(inpFilStartLern.value)) return true
    })
  }

  if (inpFilEndLern !== '') {
    studentsList = studentsList.filter(function (item) {
    if (item.endLern.includes(inpFilEndLern.value)) return true
    })
  }

  for (item of studentsList) {
    createTr(item)
  }
  table.append(tbody)
}

createStr()

filterWrapper.append(inpFilFio)
filterWrapper.append(inpFilFaculty)
filterWrapper.append(inpFilStartLern)
filterWrapper.append(inpFilEndLern)
container.append(filterWrapper)


for (i = 1; i <= 6; i++) {
  const label = document.createElement('label');
  const input = document.createElement('input')
  input.setAttribute('required', 'required')
  input.id = 'input' + i
  label.id = 'label' + i
  input.classList.add('inp-add-stud')
  form.append(label)
  const labNum = document.getElementById('label' + i)
  labNum.append(input)
}

document.getElementById('input1').placeholder = 'Имя'
document.getElementById('input2').placeholder = 'Фамилия'
document.getElementById('input3').placeholder = 'Отчество'
document.getElementById('input4').placeholder = 'Дата рождения'
document.getElementById('input5').placeholder = 'Дата начала обучения'
document.getElementById('input6').placeholder = 'Факультет'

let tik = 0;

const date = document.getElementById('input4')
const dateStart = document.getElementById('input5')
date.type = 'date'
dateStart.type = 'date'
form.append(btn)

btn.addEventListener ('click', function () {
  for (k = 1; k <= 6; k++) {
    const now = document.getElementById('input' + k)
    const value = now.value.trim()
    now.value = value
  if (value.length < 2 || value.length > 15) {
    if (document.getElementById('error' + k)) {
      break
    }
    tik++

    const labNow = document.getElementById('label' + k)
    const error = document.createElement('span')
    error.id = 'error' + k
    switch (k) {
      case 1:
        error.innerHTML = 'Имя должно иметь не менее 2-х и не более 15-ти символов';
        break;
      case 2:
        error.innerHTML = 'Фамилия должна иметь не менее 2-х и не более 15-ти символов';
        break;
      case 3:
        error.innerHTML = 'Отчество должно иметь не менее 2-х и не более 15-ти символов'
        break;
      case 4:
        error.innerHTML = 'Выберите дату рождения'
        break;
      case 5:
        error.innerHTML = 'Выберите дату начала обучения'
        break;
      case 6:
        error.innerHTML = 'Факультет должен иметь не менее 2-х и не более 15-ти символов'
        break;
    }
  labNow.append(error)
  } else {
      if (document.getElementById('error' + k)) {
      const nowError = document.getElementById('error' + k)
      nowError.remove()
      tik--
      }
    }
}
if (tik === 0) {
    let name = document.getElementById('input1').value;
    let surname = document.getElementById('input2').value;
    let lastname = document.getElementById('input3').value;
    let birthday = document.getElementById('input4').value;
    let studyStart = document.getElementById('input5').value;
    let faculty = document.getElementById('input6').value;

    createStudent(name, lastname, surname, faculty, birthday, studyStart);
    createStr()

    document.getElementById('input1').value = '';
    document.getElementById('input2').value = '';
    document.getElementById('input3').value = '';
    document.getElementById('input4').value = '';
    document.getElementById('input5').value = '';
    document.getElementById('input6').value = '';
 }
})

document.getElementById('heading1').addEventListener('click', () => {
  sortColumnFlag = 'fio'
  createStr()
})

document.getElementById('heading2').addEventListener('click', () => {
  sortColumnFlag = 'faculty'
  createStr()
})

document.getElementById('heading3').addEventListener('click', () => {
  sortColumnFlag = 'birthday'
  createStr()
})

document.getElementById('heading4').addEventListener('click', () => {
  sortColumnFlag = 'studyStart'
  createStr()
})


inpFilFio.addEventListener('input', function() {
 createStr()
})

inpFilFaculty.addEventListener('input', function () {
 createStr()
})

inpFilStartLern.addEventListener('input', function() {
 createStr()
})

inpFilEndLern.addEventListener('input', function() {
 createStr()
})
//end